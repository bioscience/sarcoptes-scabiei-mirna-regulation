library(limma)
library(edgeR)

x <- read.table("mrna.cnts", row.names=1)
colnames(x) <- c(
"eeggOo2h-1","eeggOo2h-2","eeggOo2h-3","eeggOo2h-4","eeggOo2h-5",
"leggOo2h-1","leggOo2h-2","leggOo2h-3","leggOo2h-4","leggOo2h-5",
"miteOo2h-1","miteOo2h-2","miteOo2h-3","miteOo2h-4","miteOo2h-5")

y <- as.matrix(x)
colnames(y) <- colnames(x)
rownames(y) <- rownames(x)
x <- y
x <- x[,c(-7)] # Remove leggOo2h-2 outlier

group <- as.factor(c(
"eOo2h","eOo2h","eOo2h","eOo2h","eOo2h",
"lOo2h","lOo2h","lOo2h","lOo2h",
"mOo2h","mOo2h","mOo2h","mOo2h","mOo2h"))

keep.exprs <- filterByExpr(x, group=group)
xx <- x[keep.exprs,]
dim(x)
dim(xx)
x <- xx
cps <- cpm(x)
lcps <- cpm(x, log=TRUE)
summary(lcps)

norm.factors <- calcNormFactors(x, method = "TMM")

colors <- c(1,1,1,1,1,2,2,2,2,3,3,3,3,3)
jpeg("mds.jpg", width=1024, height=768)
plotMDS(lcps, dim=c(1,2), col = colors)
title(main="Sarcoptes scabiei RNA-seq data")
dev.off()

design <- model.matrix(~0+group)
colnames(design) <- gsub("group", "", colnames(design))
design

contr.matrix <- makeContrasts(
   lOoVseOo2h = lOo2h-eOo2h,
   mOoVslOo2h = mOo2h-lOo2h,
   mOoVseOo2h = mOo2h-eOo2h,
   levels = colnames(design))

v <- voom(x, design, plot=TRUE)

vfit <- lmFit(v, design)
vfit <- contrasts.fit(vfit, contrasts=contr.matrix)
efit <- eBayes(vfit)
jpeg("sa.jpg", width=1024, height=768)
plotSA(efit)
dev.off()

summary(decideTests(efit, p.value = 0.01))

tfit <- treat(vfit, lfc=0.5)
dt <- decideTests(tfit, p.value = 0.01)
summary(dt)
res <- decideTests(tfit, p.value = 0.01)
write.table(res, "resMrnaAll.tsv", sep="\t")
