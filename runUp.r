library(svglite)
library(RColorBrewer)

# Function for permutation test
perm.cor.test <- function(x, y=NULL, alternative=c("two.sided", "less", "greater"), method=c("pearson", "spearman"), num.sim=20000 ) {
  # 'x': Numeric vector of design variable.
  # 'y': Numeric vector of response variable.
  if ( !is.numeric(x) )   stop("'x' must be numeric.")
  if ( !is.numeric(y) & !is.null(y) )   stop("'y' must be numeric or NULL.")
  if ( !is.numeric(num.sim) )  stop("'num.sim' must be numeric.")
  num.sim = floor(num.sim);   if ( num.sim < 1 )  stop("'num.sim' must be at least 1.")
  if (is.null(y))   {y <- x[,2] ;  x <- as.numeric(x[,1])}
  if (length(x)!=length(y)) stop("'x' and 'y' must have the same length.")
  if (method=="spearman")  {x <- rank(x);  y <- rank(y)}
  if (method %in% c("pearson", "spearman")) {
    test.stat0 <- cor(x,y);  test.stat <- NULL
    for (i in 1:num.sim)  {test.stat <- c(test.stat, cor(x, sample(y)))}
    if (alternative=="two.sided") p.value <- mean(abs(test.stat)>=abs(test.stat0))
    if (alternative=="less") p.value <- mean(test.stat<=test.stat0)
    if (alternative=="greater") p.value <- mean(test.stat>=test.stat0)   }
  output1 <- paste("Permutation correlation test.  Method is", method)
  output2 <- paste("p-value was estimated based on", num.sim, "simulations.")
  structure(list(output1, output2, alternative=alternative, p.value=p.value))
}	

# Read in read counts for mRNA and miRNA and convert them to log2 format 
dataR <- read.table("mrna.cnts", header=TRUE, row.names=1)
dataM <- read.table("mir.cnts", header=TRUE, row.names=1)
dataR <- log2(dataR + 1)
dataM <- log2(dataM + 1)

# Label the samples and replicates
colnames(dataM) <- c("eeggOo2h.1","eeggOo2h.2","eeggOo2h.4","leggOo2h.1","leggOo2h.2","leggOo2h.3","leggOo2h.4","leggOo2h.5","miteOo2h.1","miteOo2h.2","miteOo2h.3","miteOo2h.4","miteOo2h.5")
dataM <- dataM[,-5] # Remove outlier leggOo2h.2 because it groups with early eggs in MDS plot
dataR <- dataR[,colnames(dataM)]
pearson <- cor(t(dataR), t(dataM))
pearson[is.na(pearson)] <- 0.0

rowNames <- rownames(dataR)
colNames <- rownames(dataM)
dataR <- as.matrix(dataR)
dataM <- as.matrix(dataM)

# Permute or read in pre-permuted result file
res <- readRDS("pVals1000.rds")
#res <- matrix(nrow=length(rowNames), ncol=length(colNames))
#dim(res)
#rownames(res) <- rowNames
#colnames(res) <- colNames
#for (rowName in rowNames) {
#    for (colName in colNames) {
#            pVal <- perm.cor.test(dataR[rowName,], dataM[colName,], "two.sided", "pearson", num.sim=1000)
#            res[rowName, colName] <- pVal$p.value
#    }
#}
#saveRDS(res, file="pVals1000.rds")
res[is.na(res)] <- 1.0

pairRows <- rownames(pearson)
pairCols <- colnames(pearson)

pearsonOrig <- pearson

# Apply the criteria cor >= 0.7 and p-value <= 0.001
for (pairRow in pairRows) {
    for (pairCol in pairCols) {
        if (pearson[pairRow,pairCol] < 0.7) {
            pearson[pairRow,pairCol] <- 0.0
        }
        if (res[pairRow,pairCol] > 0.001) {
            pearson[pairRow,pairCol] <- 0.0
        }
    }
}

pearson <- pearson[pairRows,]
pearson <- pearson[,pairCols]
pearsonOrig <- pearsonOrig[pairRows,]
pearsonOrig <- pearsonOrig[,pairCols]

# Read in EdgeR differential transcription results for mRNA and miRNA
deMrna <- read.table("resMrnaAll.tsv", header=TRUE, row.names=1)
deMir <- read.table("resMirAll.tsv", header=TRUE, row.names=1)

# Evaluate miRNA upregulation to differentially transcribed genes in Ee to Le and Le to Ee directions
overlaylOoVseOo2h <- matrix(rep(0, nrow(pearson)*ncol(pearson)), nrow=nrow(pearson), ncol=ncol(pearson))
rownames(overlaylOoVseOo2h) <- rownames(pearson)
colnames(overlaylOoVseOo2h) <- colnames(pearson)
rowNamesMrna <- intersect(rownames(deMrna), rownames(pearson))
rowNamesMir <- intersect(rownames(deMir), colnames(pearson))
for (rowName in rowNamesMrna) {
    for (colName in rowNamesMir) {
        if (deMrna[rowName, "lOoVseOo2h"] == 1 && deMir[colName, "lOoVseOo2h"] == 1) {
            overlaylOoVseOo2h[rowName,colName] <- 1.0
        }
        if (deMrna[rowName, "lOoVseOo2h"] == -1 && deMir[colName, "lOoVseOo2h"] == -1) {
            overlaylOoVseOo2h[rowName,colName] <- -1.0
        }
    }
}

# Evaluate miRNA upregulation to differentially transcribed genes in Le to Af and Af to Le directions
overlaymOoVslOo2h <- matrix(rep(0, nrow(pearson)*ncol(pearson)), nrow=nrow(pearson), ncol=ncol(pearson))
rownames(overlaymOoVslOo2h) <- rownames(pearson)
colnames(overlaymOoVslOo2h) <- colnames(pearson)
rowNamesMrna <- intersect(rownames(deMrna), rownames(pearson))
rowNamesMir <- intersect(rownames(deMir), colnames(pearson))
for (rowName in rowNamesMrna) {
    for (colName in rowNamesMir) {
        if (deMrna[rowName, "mOoVslOo2h"] == 1 && deMir[colName, "mOoVslOo2h"] == 1) {
            overlaymOoVslOo2h[rowName,colName] <- 1.0
        }
        if (deMrna[rowName, "mOoVslOo2h"] == -1 && deMir[colName, "mOoVslOo2h"] == -1) {
            overlaymOoVslOo2h[rowName,colName] <- -1.0
        }
    }
}

# Evaluate miRNA upregulation to differentially transcribed genes in Ee to Af and Af to Ee directions
overlaymOoVseOo2h <- matrix(rep(0, nrow(pearson)*ncol(pearson)), nrow=nrow(pearson), ncol=ncol(pearson))
rownames(overlaymOoVseOo2h) <- rownames(pearson)
colnames(overlaymOoVseOo2h) <- colnames(pearson)
rowNamesMrna <- intersect(rownames(deMrna), rownames(pearson))
rowNamesMir <- intersect(rownames(deMir), colnames(pearson))
for (rowName in rowNamesMrna) {
    for (colName in rowNamesMir) {
        #if (deMrna[rowName, "mOoVseOo2h"] * deMir[colName, "mOoVseOo2h"] == -1) {
        #    overlaymOoVseOo2h[rowName,colName] <- 1.0
        #}
        #overlaymOoVseOo2h[rowName,colName] <- deMrna[rowName, "mOoVseOo2h"] * deMir[colName, "mOoVseOo2h"]
        if (deMrna[rowName, "mOoVseOo2h"] == 1 && deMir[colName, "mOoVseOo2h"] == 1) {
            overlaymOoVseOo2h[rowName,colName] <- 1.0
        }
        if (deMrna[rowName, "mOoVseOo2h"] == -1 && deMir[colName, "mOoVseOo2h"] == -1) {
            overlaymOoVseOo2h[rowName,colName] <- -1.0
        }
    }
}

# Calculate dendrograms to genes and miRNAs
rowD <- as.dendrogram(hclust(dist(pearsonOrig)))
colD <- as.dendrogram(hclust(dist(t(pearsonOrig))))

# Create heatmaps
colors <- c("#e82a60","#FFFFFF","#741530")
jpeg("heatmaplOoVseOo2hUp.jpg", width=2400, height=2000)
heatmap(overlaylOoVseOo2h * pearson * -1.0, scale="none", Rowv=rowD, Colv=colD, col=colors)
dev.off()
jpeg("heatmapmOoVslOo2hUp.jpg", width=2400, height=2000)
heatmap(overlaymOoVslOo2h * pearson * -1.0, scale="none", Rowv=rowD, Colv=colD, col=colors)
dev.off()
jpeg("heatmapmOoVseOo2hUp.jpg", width=2400, height=2000)
heatmap(overlaymOoVseOo2h * pearson * -1.0, scale="none", Rowv=rowD, Colv=colD, col=colors)
dev.off()

svglite("heatmaplOoVseOo2hUp.svg", width=12, height=12)
heatmap(overlaylOoVseOo2h * pearson * -1.0, scale="none", Rowv=rowD, Colv=colD, col=colors)
dev.off()
svglite("heatmapmOoVslOo2hUp.svg", width=12, height=12)
heatmap(overlaymOoVslOo2h * pearson * -1.0, scale="none", Rowv=rowD, Colv=colD, col=colors)
dev.off()
svglite("heatmapmOoVseOo2hUp.svg", width=12, height=12)
heatmap(overlaymOoVseOo2h * pearson * -1.0, scale="none", Rowv=rowD, Colv=colD, col=colors)
dev.off()

# Save the data
data <- NULL
cnt <- 1
mat <- overlaylOoVseOo2h * abs(pearson)
for (colName in colnames(overlaylOoVseOo2h)) {
    for (rowName in rownames(overlaylOoVseOo2h)) {
	if (mat[rowName, colName] != 0) {
            data <- rbind(data, mat[rowName, colName])
            rownames(data)[cnt] <- paste(rowName, colName, sep=" ")
            cnt <- cnt + 1
	}
    }
}

data <- NULL
cnt <- 1
mat <- overlaymOoVslOo2h * abs(pearson)
for (colName in colnames(overlaymOoVslOo2h)) {
    for (rowName in rownames(overlaymOoVslOo2h)) {
	if (mat[rowName, colName] != 0) {
            data <- rbind(data, mat[rowName, colName])
            rownames(data)[cnt] <- paste(rowName, colName, sep=" ")
            cnt <- cnt + 1
	}
    }
}
write.table(data, file="demOoVslOo2hUp.res", row.names=TRUE, col.names=FALSE)

data <- NULL
cnt <- 1
mat <- overlaymOoVseOo2h * abs(pearson)
for (colName in colnames(overlaymOoVseOo2h)) {
    for (rowName in rownames(overlaymOoVseOo2h)) {
	if (mat[rowName, colName] != 0) {
            data <- rbind(data, mat[rowName, colName])
            rownames(data)[cnt] <- paste(rowName, colName, sep=" ")
            cnt <- cnt + 1
	}
    }
}
write.table(data, file="demOoVseOo2hUp.res", row.names=TRUE, col.names=FALSE)

