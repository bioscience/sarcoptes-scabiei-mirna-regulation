# Sarcoptes scabiei miRNA regulation

R-codes for the publication "Evidence that transcriptional alterations in Sarcoptes scabiei are under tight post-transcriptional (microRNA) control".

## Description of Files
* `runDeMrna.r`: differential transcription for mRNA data using the software EdgeR
* `runDeMir.r`: differential transcription for miRNA data using the software EdgeR
* `runDn.r`: inferring genes downregulated by miRNAs using correlation analysis (generates heatmap images)
* `runUp.r`: inferring genes upregulated by miRNAs using correlation analysis (generates heatmap images)
* `mrna.cnts`: read count data for mRNA samples and replicates
* `mir.cnts`: read count data for miRNA samples and replicates
* `pVals1000.rds`: precalculated p-values for each miRNA-gene correlation
* `mir.gff`: GFF file to locate miRNAs in the genome




